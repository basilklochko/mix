﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Mixed ASP.NET MVC & Angular Application";

            return View();
        }

        public ActionResult Angular()
        {
            return View();
        }
    }
}