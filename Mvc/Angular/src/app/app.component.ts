import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  public isSubComponentVisible: boolean;

  public loadSubComponent(): void {
    this.isSubComponentVisible = !this.isSubComponentVisible;
  }
}
